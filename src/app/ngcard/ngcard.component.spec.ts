import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NgcardComponent } from './ngcard.component';

describe('NgcardComponent', () => {
  let component: NgcardComponent;
  let fixture: ComponentFixture<NgcardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NgcardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NgcardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
