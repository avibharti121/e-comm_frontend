import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import{FormGroup,FormControl,FormBuilder,Validators} from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  login!: FormGroup;
  submitted:boolean=false;
  valid=false;

  constructor(
    private fb:FormBuilder,
    private http:HttpClient,
    private router:Router

    
    ) { }

  ngOnInit(): void {
    this.initForm();
  }

  initForm(){
    this.login = this.fb.group({
      email:['',[Validators.required,Validators.pattern(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/)]],
      password:['',[Validators.required,Validators.pattern(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/)]]
    })
    console.log(this.login);
  }

  onSubmit(){
    console.log(this.login);
    this.submitted=true;
    if(!this.login.invalid){
      console.log(this.login.value);
      this.http.post('http://localhost:8800/login', this.login.value).subscribe((res:any) => {
        console.log(res);
        alert(res.message);
      },err => {
        console.log(err);
        alert(err.error.message); 
      });
      
    }  
  }
}