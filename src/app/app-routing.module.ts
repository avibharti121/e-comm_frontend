import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CarouselComponent } from './carousel/carousel.component';
import { LoginComponent } from './login/login.component';
import { ModalComponent } from './modal/modal.component';
import { NgcardComponent } from './ngcard/ngcard.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { ProductComponent } from './product/product.component';
import { SignupComponent } from './signup/signup.component';

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  {path:'login',component:LoginComponent},
  { path: 'layout', loadChildren: () => import('./module/module.module').then(m => m.ModuleModule)},
  {path:'signup',component:SignupComponent},
  {path:'ngcard',component:NgcardComponent},
  {path:'product',component:ProductComponent},
  {path:'modal',component:ModalComponent},
  {path:'carousel',component:CarouselComponent},


  {path:'**',component:PageNotFoundComponent}
 

]
  
  // { path: 'login', component: LoginComponent, canActivate: [ExternalGuard]},


  

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
