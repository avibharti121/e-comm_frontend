import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import{FormGroup,FormControl,FormBuilder,Validators} from '@angular/forms';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  signup!: FormGroup;
  submitted:boolean=false;
  trueconfirmpassword=false;
  valid=false;
  constructor(
    private http:HttpClient,
    private formbuilder:FormBuilder) { }

  ngOnInit(): void {
    this.initForm();
    console.log("Signup is loaded")
  }
  initForm(){
    this.signup=this.formbuilder.group({
    name:['',[Validators.required,Validators.minLength(3),Validators.maxLength(20),Validators.pattern('[a-zA-Z]*')]],
    email:['',[Validators.required,Validators.pattern(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/)]],
    terms:['',[Validators.requiredTrue]],
    password:['',[Validators.required ,Validators.minLength(6),Validators.maxLength(30),Validators.pattern(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/)]],
    confirmPassword:['',[Validators.required]]},
      {validator: ConfirmedValidator('password', 'confirmPassword')
    })
    console.log(this.signup)
    function ConfirmedValidator(controlName: string, matchingControlName: string){
      return (formGroup: FormGroup) => {
          const control = formGroup.controls[controlName];
          const matchingControl = formGroup.controls[matchingControlName];
          if (matchingControl.errors && !matchingControl.errors.confirmedValidator) {
              return;
          }
          if (control.value !== matchingControl.value) {
              matchingControl.setErrors({ confirmedValidator: true });
          } else {
              matchingControl.setErrors(null);
          }
      }
    }
  }
  onSubmit(){
 
    if (!this.signup.invalid){
      console.log(this.signup.value);
      this.http.post('http://localhost:8800/register', this.signup.value).subscribe((res:any) =>{
        console.log(res);
        console.log(res.status);
        alert(res.message);
      }, err => {
        console.log(err);
        alert(err.error.message);
      });
          }
          
        } 
      }
      
    
